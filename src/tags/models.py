from django.db import models

# Create your models here.

class ImageTag(models.Model):
    """Model for tags that are associated with an image."""
    
    # The text of the tag.
    text = models.CharField(max_length=60)

    images = models.ManyToManyField("images.Image", related_name="tags", blank=True)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("imagetag_detail", kwargs={"pk": self.pk})


class LexemeTag(models.Model):
    """Model for tags that are associated with a lexeme."""
    
    # The text of the tag.
    text = models.CharField(max_length=60)

    lexemes = models.ManyToManyField("languages.Lexeme", related_name="tags", blank=True)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("lexemetag_detail", kwargs={"pk": self.pk})



class SentenceTag(models.Model):
    """Model for tags that are associated with a sentence."""
    
    # The text of the tag.
    text = models.CharField(max_length=60)

    sentences = models.ManyToManyField("sentences.Sentence", related_name="tags", blank=True)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("sentencetag_detail", kwargs={"pk": self.pk})


class AudioTag(models.Model):
    """Model for tags that are associated with an audio."""
    
    # The text of the tag.
    text = models.CharField(max_length=60)

    audios = models.ManyToManyField("sentences.Audio", related_name="tags", blank=True)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("sentencetag_detail", kwargs={"pk": self.pk})
