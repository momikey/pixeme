from django.urls import path

from . import views

app_name = "tags"
urlpatterns = [
    # The main tag page, showing the image, its sentences and audios, and so on.
    path("<str:text>/", views.tag_page, name="tag_page"),

    # HTML partials
]
