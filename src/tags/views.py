from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse

from .models import AudioTag, ImageTag, LexemeTag, SentenceTag

# Create your views here.

def tag_page(request: HttpRequest, text: str):
    """Get the home page for a tag."""

    image_tag = ImageTag.objects.filter(text=text).first()
    lexeme_tag = LexemeTag.objects.filter(text=text).first()
    sentence_tag = SentenceTag.objects.filter(text=text).first()
    audio_tag = AudioTag.objects.filter(text=text).first()

    if not any((image_tag, lexeme_tag, sentence_tag, audio_tag)):
        raise Http404(f"No items tagged with {text}")

    context = {
        "tag": text,
        "tagged_images": image_tag.images if image_tag is not None else None,
        "tagged_lexemes": lexeme_tag.lexemes if lexeme_tag is not None else None,
        "tagged_sentences": sentence_tag.sentences if sentence_tag is not None else None,
        "tagged_audios": audio_tag.audios if audio_tag is not None else None,
    }

    return TemplateResponse(request, "tags/tag_view.jinja", context)
