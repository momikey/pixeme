from django.contrib import admin

from .models import AudioTag, ImageTag, LexemeTag, SentenceTag

# Register your models here.
admin.site.register((AudioTag, ImageTag, LexemeTag, SentenceTag))
