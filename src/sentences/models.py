from base64 import urlsafe_b64encode
from django.db import models
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext as _

from django_extensions.db.models import TimeStampedModel

from simpleflake import simpleflake

from comments.models import CommentMixin

# Create your models here.

def _upload_path_for_audio(instance, filename):
    flake_id = simpleflake()
    flake_str = urlsafe_b64encode(flake_id.to_bytes(9, "big")).decode()
    return f"user/{instance.uploader.id}/audios/{flake_str}_{filename}"


class Sentence(TimeStampedModel, CommentMixin, models.Model):
    """Model for a sentence. Sentences are associated with images, not lexemes,
    and can have accompanying audios."""

    class SentenceStatus(models.TextChoices):
        APPROVED = "APP", _("Approved")
        REJECTED = "REJ", _("Rejected")
        WAITING = "WAI", _("Waiting")
        QUESTIONED = "QUE", _("Questioned")
        FLAGGED = "FLG", _("Flagged for Review")

    # The text of the sentence.
    text = models.TextField()

    # The status of this sentence, which determines if/how it is shown to users.
    status = models.CharField(max_length=3, choices=SentenceStatus.choices, default=SentenceStatus.WAITING)

    # Whether this sentence is considered NSFW, and thus hidden from unverified users.
    nsfw = models.BooleanField(default=False)

    # A difficulty rating for the sentence, from 1-5.
    # These roughly correspond to the A1, A2, B1, B2, and C1 levels of language instruction courses.
    difficulty = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], default=3)

    # The language of this sentence.
    language = models.ForeignKey("languages.Language", on_delete=models.CASCADE, related_name="sentences")

    # The image to which this sentence is attached.
    image = models.ForeignKey("images.Image", on_delete=models.CASCADE, related_name="sentences")

    # User who submitted the sentence. This is usually, but not always, the image uploader.
    submitter = models.ForeignKey(User, on_delete=models.CASCADE, related_name="submitted_sentences")

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("sentence_detail", kwargs={"pk": self.pk})


class Audio(TimeStampedModel, CommentMixin, models.Model):
    """Model for a user-uploaded audio corresponding to a sentence.
    
    Audios don't have statuses of their own. Instead, they are shown/hidden
    based on the status of their corresponding sentence. This includes NSFW status.
    """

    # The URI for the audio. This is relative to a storage backend, for the same
    # reasons as in the Image class.
    # uri = models.CharField(max_length=250)
    upload = models.FileField(upload_to=_upload_path_for_audio, max_length=255)

    # The user who uploaded the audio.
    uploader = models.ForeignKey(User, on_delete=models.CASCADE, related_name="uploaded_audios")

    # Unstructured metadata about the image.
    audio_meta = models.JSONField(encoder=DjangoJSONEncoder, default=dict, null=True, blank=True)

    # The sentence this audio is attached to.
    sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE, related_name="audios")

    # User who uploaded the audio. This is usually, but not always, the one who submitted the sentence.
    uploader = models.ForeignKey(User, on_delete=models.CASCADE, related_name="uploaded_audios")

    def __str__(self):
        return self.upload.url

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("audio_detail", kwargs={"pk": self.pk})
