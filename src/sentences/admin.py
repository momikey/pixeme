from django.contrib import admin

from .models import Audio, Sentence

# Register your models here.
admin.site.register([Audio, Sentence])
