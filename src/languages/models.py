import re

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from anyascii import anyascii

# Create your models here.

# Regex pattern for custom, private-use language codes.
PRIVATE_USE_CHARS = re.compile(r"[^a-z0-9]+")

class Language(models.Model):
    """Model class for a language, whether natural or constructed."""

    class LanguageType(models.TextChoices):
        NATURAL = "NAT", _("Natural Language")
        CONLANG = "CON", _("Constructed Language")
        SIGNLANG = "SGN", _("Sign Language")
        AUXLANG = "AUX", _("Auxiliary Language")
        ARTLANG = "ART", _("Artistic Language")
        ENGLANG = "ENG", _("Engineered Language")

    # The English name of a language.
    name = models.CharField(max_length=60)

    # The name of a language in that language.
    native = models.CharField(max_length=80)

    # The most common IETF language tag for this language.
    # (see RFC 5646 and BCP 47)
    # Constructed languages that aren't popular enough to be assigned a code
    # will use the "private use" codes `qca-qcz`, with the final letter of the
    # code taken from the first letter of the language's English name.
    # This is then followed by a private subtag containing up to the first eight
    # letters of the language's English name in lower case, with non-ASCII
    # characters converted into their closest English approximations.
    # Exs: `qcd-x-dothraki`, `qci-x-ithkuil`, `qck-x-khangath`
    # In the rare event that two languages would have the same code by this method,
    # a different primary code is used: `qdx` for the first duplicate, `qex` for
    # the second, and so on until `qtx`.
    # If more than 18 languages require the same prefix, we'll need a new method.
    language_code = models.CharField(max_length=40, unique=True)

    # The type of language. Most of these will be natural languages, obviously.
    language_type = models.CharField(max_length=3, choices=LanguageType.choices, default=LanguageType.NATURAL)

    # If this language is a dialect or regional variant of another, then
    # connect it to its parent language.
    parent = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, related_name="children")

    # A language can have a sponsor and multiple supporters. These are users who
    # have paid for higher levels of access.
    sponsor = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="sponsored_languages")
    supporters = models.ManyToManyField(User, related_name="supported_languages", blank=True)

    # Users who have claimed fluency in this language.
    speakers = models.ManyToManyField(User, through="users.Fluency", related_name="fluencies", blank=True)

    class Meta:
        indexes = [
            models.Index(fields=["language_code"])
        ]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("languages:detail", kwargs={"code": self.language_code})

    def is_conlang(self):
        """Whether this language is a constructed language of any type other than sign languages."""

        return self.language_type not in { self.LanguageType.NATURAL, self.LanguageType.SIGNLANG }

    @staticmethod
    def code_for_language(name: str) -> str:
        """Generate a language code for a conlang by taking the first 8 characters
        of its representation in ASCII, with all non-alphanumerics removed.
        
        Note that this does not take into account duplicate name prefixes.
        """

        # Transliterate Unicode characters to ASCII, then change to lowercase.
        name_without_unicode = anyascii(name).lower()

        return re.sub(PRIVATE_USE_CHARS, "", name_without_unicode)[:8]

    @property
    def ancestor_chain(self):
        """Get a list of this language and all its ancestors (parent, parent's parent, etc.)."""

        # We could recurse, but iteration is faster and simpler.
        ancestors = [self]
        next = self.parent

        while next is not None:
            ancestors.append(next)
            next = next.parent

        return ancestors

class Lexeme(models.Model):
    """Model for a single lexeme. These are always attached to a language."""

    # The text of the lexeme itself. This can be in any Unicode-supported script.
    text = models.CharField(max_length=160)

    # A transliteration of the lexeme into Latin characters, for lexemes written
    # in other scripts.
    transliteration = models.CharField(max_length=200, null=True, blank=True)

    # An IPA representation of the lexeme. This isn't required, but it is recommended.
    ipa = models.CharField(max_length=160, null=True, blank=True)

    # Whether this lexeme needs to be hidden for logged-out users.
    warning = models.BooleanField(_("Content warning"), default=False)

    # The language this lexeme is in. This can be any supported language, natural or constructed.
    language = models.ForeignKey(Language, on_delete=models.CASCADE, related_name="lexemes")

    class Meta:
        indexes = [
            models.Index(fields=["text", "language"])
        ]
        constraints = [
            models.UniqueConstraint(fields=["language", "text"], name="unique_lexeme")
        ]

    def __str__(self):
        return f"{self.language.language_code}:{self.text}"

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("languages:lexeme_detail", kwargs={"text": self.text, "code": self.language.language_code})


class Definition(models.Model):
    """Model for a definition of a lexeme."""

    lexeme = models.ForeignKey(Lexeme, on_delete=models.CASCADE, related_name="definitions")

    # The actual text of the definition.
    text = models.TextField(_("Definition Text"))

    # The language this definition is in. This defaults to that of the lexeme,
    # but it can be different to allow, e.g., translation help.
    language = models.ForeignKey(Language, verbose_name=_("Definition Language"), on_delete=models.CASCADE)

    # Translation or usage notes. These are optional, and will be shown as a footnote.
    notes = models.TextField(_("Usage Notes"), null=True, blank=True)

    class Meta:
        indexes = [
            models.Index(fields=["lexeme"])
        ]

    def __str__(self):
        tr = self.text[:20] + ("..." if len(self.text) > 20 else "")
        return f"{tr} : {self.language.language_code}:{self.text}"

    def get_absolute_url(self):
        from django.urls import reverse

        # Definitions don't have their own page for now. Instead, they go to the lexeme's page.
        return reverse("languages:lexeme_detail", kwargs={"text": self.lexeme.text, "code": self.lexeme.language})
