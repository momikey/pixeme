from django.urls import path

from . import views

app_name = "languages"
urlpatterns = [
    # List of languages. (Later, we'll paginate or filter or something.)
    path("", views.languages_home, name="index"),
    # Single language view.
    path("<str:code>/", views.language_detail, name="detail"),

    # Single lexeme view.
    path("<str:code>/<str:text>/", views.lexeme_detail, name="lexeme_detail"),
]
