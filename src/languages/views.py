from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse

from .models import Language, Lexeme

from images.services import visible_images_for_user

# Create your views here.
def languages_home(request: HttpRequest):
    """View for the home page of the languages app.

    This should show a list of known languages, along with some stats.
    """

    total_languages = Language.objects.count()
    newest_languages = Language.objects.order_by("-pk")[:10]

    context = {
        "total_languages": total_languages,
        "newest_languages": newest_languages,
    }
    return TemplateResponse(request, "languages/index.jinja", context)


def language_detail(request: HttpRequest, code: str):
    """View for detail of a single language."""

    language = get_object_or_404(Language, language_code=code)

    # TODO: Show lexemes in daughter languages.

    newest = {
        "lexemes": language.lexemes.order_by("-pk")[:10],
        "sentences": language.sentences.order_by("-created")[:10],
    }
    
    context = {
        "language": language,
        "newest": newest,
    }
    return TemplateResponse(request, "languages/detail.jinja", context)


def lexeme_detail(request: HttpRequest, text: str, code: str):
    """View for detail of a single lexeme."""

    lexeme = get_object_or_404(Lexeme, text=text, language__language_code=code)

    # The imgages for this lexeme need to be filtered, so break them into their own object.
    images = visible_images_for_user(request.user, lexeme.images).order_by("-created")

    # Group the definitions by language for easier looping in the template.
    definitions = {
        "native": lexeme.definitions.filter(language__language_code=code),
        "other": lexeme.definitions.exclude(language__language_code=code),
    }

    context = {
        "lexeme": lexeme,
        "definitions": definitions,
        "images": images,
    }
    return TemplateResponse(request, "languages/lexeme.jinja", context)
