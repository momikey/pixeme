from django.contrib.auth.models import User

from .models import Language


def visible_language(user: User, request_language: str = None) -> Language:
    """Get the language that should be visible for the given user."""

    if request_language is not None:
        # If there's a requested language code, try to use that.
        try:
            return Language.objects.get(language_code=request_language)
        except Language.DoesNotExist:
            # If the requested language doesn't exist, try to find a fallback.
            pass

    if user.is_authenticated and user.profile is not None and user.profile.default_language:
        # If the user is logged in and has a profile, use the language set there.
        return user.profile.default_language
    else:
        # Fallback is English, since the current session language would be
        # handled by the view and passed in as the request language.
        return Language.objects.get(language_code="en")
