from django.contrib import admin

from .models import Definition, Language, Lexeme

# Register your models here.
admin.site.register([Definition, Language, Lexeme])
