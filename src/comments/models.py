from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from django_extensions.db.models import TimeStampedModel

# Create your models here.

# For the same reasons as in the lists app, we have to use an inheritance
# structure for comments, because they can be on different types of objects.

class Comment(TimeStampedModel, models.Model):
    """Abstract model for a comment of any type."""

    class CommentStatus(models.TextChoices):
        PUBLIC = "PUB", _("Public")
        DRAFT = "DRF", _("Draft")
        HELD = "HLD", _("Held")
        DELETED = "XXX", _("Deleted")

    # Text of the comment.
    text = models.TextField()

    # Status of the comment.
    status = models.CharField(max_length=3, choices=CommentStatus.choices, default=CommentStatus.DRAFT)

    # The comment's submitter.
    # Because of the way Django naming works, we don't specify a `related_name` here.
    # Instead, subclasses will set the `default_related_name` meta option.
    # Thanks to https://stackoverflow.com/a/52851434.
    submitter = models.ForeignKey(User, on_delete=models.CASCADE)

    # An optional parent for the comment, to allow for threading.
    # All comments in a thread must be of the same type, so we can't put the parent here.

    class Meta:
        abstract = True

    def __str__(self):
        return f"Comment {self.pk} by {self.submitter.username}"


class ImageComment(Comment):
    """Model for a comment on an image."""

    image = models.ForeignKey("images.Image", on_delete=models.CASCADE, related_name="comments")

    class Meta:
        default_related_name = "image_comments"

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("image_comment_detail", kwargs={"pk": self.pk})


class SentenceComment(Comment):
    """Model for a comment on a sentence."""

    sentence = models.ForeignKey("sentences.Sentence", on_delete=models.CASCADE, related_name="comments")

    class Meta:
        default_related_name = "sentence_comments"

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("sentence_comment_detail", kwargs={"pk": self.pk})


class AudioComment(Comment):
    """Model for a comment on an audio."""

    audio = models.ForeignKey("sentences.Audio", on_delete=models.CASCADE, related_name="comments")

    class Meta:
        default_related_name = "audio_comments"

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("audio_comment_detail", kwargs={"pk": self.pk})


class CommentMixin:
    """Mixin class to add some properties that are shared by all models which have comments."""

    @property
    def public_comments(self):
        """Set of only comments that have been posted publicly, and aren't currently under moderation."""
        return self.comments.filter(status=Comment.CommentStatus.PUBLIC)

