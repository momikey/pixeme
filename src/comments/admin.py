from django.contrib import admin

from .models import AudioComment, ImageComment, SentenceComment

# Register your models here.
admin.site.register([AudioComment, ImageComment, SentenceComment])