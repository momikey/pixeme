# Generated by Django 4.2.10 on 2024-02-21 18:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sentences', '0002_alter_audio_options_alter_audiotag_options_and_more'),
        ('images', '0002_image_lexeme'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SentenceComment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('text', models.TextField()),
                ('status', models.CharField(choices=[('PUB', 'Public'), ('DRF', 'Draft'), ('HLD', 'Held'), ('XXX', 'Deleted')], default='DRF', max_length=3)),
                ('sentence', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='sentences.sentence')),
                ('submitter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_related_name': 'sentence_comments',
            },
        ),
        migrations.CreateModel(
            name='ImageComment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('text', models.TextField()),
                ('status', models.CharField(choices=[('PUB', 'Public'), ('DRF', 'Draft'), ('HLD', 'Held'), ('XXX', 'Deleted')], default='DRF', max_length=3)),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='images.image')),
                ('submitter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_related_name': 'image_comments',
            },
        ),
        migrations.CreateModel(
            name='AudioComment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('text', models.TextField()),
                ('status', models.CharField(choices=[('PUB', 'Public'), ('DRF', 'Draft'), ('HLD', 'Held'), ('XXX', 'Deleted')], default='DRF', max_length=3)),
                ('audio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='sentences.audio')),
                ('submitter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_related_name': 'audio_comments',
            },
        ),
    ]
