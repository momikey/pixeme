"""
URL configuration for pixeme project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from . import views

urlpatterns = [
    # Paths from 3rd party extensions.
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path("__reload__/", include("django_browser_reload.urls")),

    # Static handling for uploads.

    # Our paths.
    path("", views.home_page, name="index"),

    # Include paths for each app, too.
    path("image/", include("images.urls")),
    path("lang/", include("languages.urls")),
    path("tags/", include("tags.urls")),
    path("users/", include("users.urls")),
]

# This should be dev-mode only.
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
