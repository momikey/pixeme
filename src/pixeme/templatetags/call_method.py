# Because Django's template system is intentionally gimped, 
# (and we can't switch to Jinja project-wide because of deps)
# we have to add this hack to be able to call methods in templates.
# It creates a new tag used as: {% call_method obj 'method' args... %}.

from django import template

register = template.Library()

@register.simple_tag
def call_method(obj, method_name, *args):
    method = getattr(obj, method_name)
    return method(*args)
