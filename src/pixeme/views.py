# Views for the "base" app: anything that doesn't go in another app.

from django.http import HttpRequest
from django.template.response import TemplateResponse

def home_page(request: HttpRequest):
    """View for the Pixeme home page."""

    context = {}
    return TemplateResponse(request, "pages/home.jinja", context)
