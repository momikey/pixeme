from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse

# Create your views here.

def user_profile(request: HttpRequest, username: str):
    """Show the profile page for a user. This should have different appearances based on
    whether the viewer is logged in and whether he is viewing his own profile."""

    user = get_object_or_404(User, username=username)

    same_user = (request.user.is_authenticated and request.user == user)

    context = {
        "profile_user": user,
        "same_user": same_user,
    }

    return TemplateResponse(request, "users/profile.jinja", context)
