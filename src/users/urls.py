from django.urls import path

from . import views

app_name = "users"
urlpatterns = [
    # The main image page, showing the image, its sentences and audios, and so on.
    path("<str:username>/", views.user_profile, name="profile")
]
