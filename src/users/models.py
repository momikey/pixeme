from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext as _

from django_extensions.db.models import TimeStampedModel

# Create your models here.

class Profile(TimeStampedModel, models.Model):
    """Model of a user profile. These are attached to users in a one-to-one
    relationship, but kept in a separate table because Django already has a
    User model for authentication. So this is basically the account/profile
    distinction we would've used anyway."""

    # The user owning this profile.
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")

    # A simple trust level score for users. This increases as they post good images,
    # sentences, and audios. It decreases if their images are flagged for review
    # or violations of site rules.
    trust = models.IntegerField(default=0, verbose_name=_("Trust score"))

    # The default language for the user when visiting the site, and for any data
    # that user submits, such as lexemes and sentences.
    # If this isn't set, then the language defaults to the current language of the site,
    # which starts as English but users may choose a different one later.
    default_language = models.ForeignKey("languages.Language", verbose_name=_("Default language"), null=True, blank=True, on_delete=models.SET_NULL)

    # Whether the user wants to see NSFW images. This is opt-in.
    show_nsfw = models.BooleanField(_("Show NSFW"), default=False)

    def __str__(self):
        return f"Profile for {self.user.username}"

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("profile_detail", kwargs={"pk": self.pk})


class Notification(TimeStampedModel, models.Model):
    """Model for a notification sent by the system to a user."""
    
    # Text of the notification. These are all system-generated.
    text = models.TextField()

    # Whether the user has read the notification.
    is_read = models.BooleanField(default=False, verbose_name=_("Notification read"))

    # A link from the notification to an associated page.
    # Ex: A notification for a new comment on a sentence would link to the sentence's page.
    link = models.URLField(max_length=200)

    # The user receiving this notification.
    # Note that there are no user-to-user notifications, so we don't need to track the sender.
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notifications")

    def __str__(self):
        return f"Notification {self.pk} for {self.user.username}"

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("notification_detail", kwargs={"pk": self.pk})


class Fluency(TimeStampedModel, models.Model):
    """Model for a user's fluency in a language."""
    
    # This is effectively a many-to-many relation with extra data attached.

    # The fluency level of the user in the given language.
    # These roughly correspond to the A1, A2, B1, B2, and C1 levels of common language courses.
    level = models.SmallIntegerField(verbose_name=_("Fluency level"), validators=[MinValueValidator(1), MaxValueValidator(5)], default=1)

    # The sides of the many-to-many relation: user and language.
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    language = models.ForeignKey("languages.Language", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Fluency")
        verbose_name_plural = _("Fluencies")
        constraints = [
            models.UniqueConstraint(fields=["user", "language"], name="unique_user_fluency")
        ]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("fluency_detail", kwargs={"pk": self.pk})

