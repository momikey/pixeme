from django.contrib import admin

from .models import Fluency, Notification, Profile

# Register your models here.
admin.site.register([Fluency, Notification, Profile])
