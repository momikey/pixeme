from django.urls import path

from . import views

app_name = "images"
urlpatterns = [
    # The main image page, showing the image, its sentences and audios, and so on.
    path("<str:hash_id>/", views.image_detail, name="image_page"),

    # HTML partials

    path("<str:hash_id>/partial/lang/", views.image_data_per_language, name="image_data_partial"),
]
