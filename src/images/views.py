from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from languages.models import Language

from languages.services import visible_language

from .services import image_lexemes_by_language, image_sentences_by_language

from .models import Image

# Create your views here.

def image_detail(request: HttpRequest, hash_id: str):
    """Get the main detail page for an image."""

    image = get_object_or_404(Image, pk=Image.unhash_id(hash_id))

    languages_by_lexeme = image_lexemes_by_language(image)
    current_language = visible_language(request.user, request.session.get("lang"))
    context = {
        "image": image,
        "current_language": current_language,
        "lexemes": languages_by_lexeme[current_language],
        "languages": languages_by_lexeme.keys(),
        "sentences": image_sentences_by_language(image)[current_language],
    }
    return TemplateResponse(request, "images/image.jinja", context)


def image_data_per_language(request: HttpRequest, hash_id: str):
    """Get lexeme and sentence data for an image, returned as structured HTML."""

    try:
        code = request.GET["lang"]
    except KeyError:
        raise Http404("No language provided")

    image = get_object_or_404(Image, pk=Image.unhash_id(hash_id))
    current_language = Language.objects.get(language_code=code)
    lexemes = image_lexemes_by_language(image)[current_language]
    sentences = image_sentences_by_language(image)[current_language]

    context = {
        "image": image,
        "lexemes": lexemes,
        "sentences": sentences,
        "current_language": current_language,
    }
    print(context)
    return TemplateResponse(request, "partials/lexeme_sentence_list.jinja", context)
