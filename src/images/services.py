from collections import defaultdict
from typing import Dict, List
from django.contrib.auth.models import User
from django.db.models import Q, QuerySet
from django.db.models.manager import BaseManager

from more_itertools import map_reduce

from .models import Image
from languages.models import Language, Lexeme
from sentences.models import Sentence

def visible_images_for_user(user: User, image_set: BaseManager) -> QuerySet:
    """Filter out the images that the given user is not allowed (or chooses not) to access."""

    # Only the uploader gets to see private images. Nobody gets to see ones that are flagged.
    filtered_query_set = image_set.get_queryset().filter(
        Q(uploader__pk=user.pk) | Q(private=False),
        status=Image.ImageStatus.ACCEPTED
    )

    if not user.is_authenticated:
        # Guests don't get to see NSFW or anything still in the moderation queue.
        filtered_query_set = filtered_query_set.exclude(nsfw=True)
    else:
        # Logged-in users can choose whether to see NSFW images.
        if not user.profile.show_nsfw:
            filtered_query_set = filtered_query_set.exclude(nsfw=True)

    # Any other filtering we want to do here...

    return filtered_query_set


def image_lexemes_by_language(image: Image) -> Dict[Language, List[Lexeme]]:
    """Break down the list of an image's lexemes into a dictionary keyed on language code."""

    # Also handle dialects by putting items into the parent language as well.
    # If there's an itertools recipe for this, I'd love to know.
    # return map_reduce(image.lexemes.all(), lambda l: l.language)
    pairs = [(l, l.language.ancestor_chain) for l in image.lexemes.all()]
    result = defaultdict(list)
    for lex, langs in pairs:
        for lang in langs:
            result[lang].append(lex)
    return result


def image_sentences_by_language(image: Image) -> Dict[Language, List[Sentence]]:
    """Break down the list of an image's sentences into a dictionary keyed on the sentence's language code."""

    # Same as for lexemes, including the request for an itertools recipe.
    # return map_reduce(image.sentences.all(), lambda s: s.language)
    pairs = [(s, s.language.ancestor_chain) for s in image.sentences.all()]
    result = defaultdict(list)
    for sentence, langs in pairs:
        for lang in langs:
            result[lang].append(sentence)
    return result
