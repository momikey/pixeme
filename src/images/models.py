from base64 import urlsafe_b64encode
from django.db import models
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.translation import gettext as _

from django_extensions.db.models import TimeStampedModel
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit

from hashids import Hashids
from simpleflake import simpleflake

from django.conf import settings

from comments.models import CommentMixin
from sentences.models import Sentence

# Create your models here.

def _upload_path_for_image(instance, filename):
    flake_id = simpleflake()
    flake_str = urlsafe_b64encode(flake_id.to_bytes(9, "big")).decode()
    return f"user/{instance.uploader.id}/images/{flake_str}_{filename}"


_image_hashid_generator = Hashids(salt="Pixeme Images", min_length=4, alphabet=settings.HASHID_ALPHABET)

class Image(TimeStampedModel, CommentMixin, models.Model):
    """Model for a user-uploaded image that can be accompanied by sentencces and audios."""

    class ImageStatus(models.TextChoices):
        ACCEPTED = "ACC", _("Accepted")
        MODERATED = "MOD", _("In Moderation")
        FLAGGED = "FLG", _("Flagged for Violation")
        DISPUTED = "DIS", _("Subject to Legal Dispute")

    # The URI for this image; this needs to be relative to a storage backend, in case
    # we need to move things around later.
    # NOTE: This means we can't use URLField, because that expects *absolute* URLs.
    # uri = models.CharField(max_length=250)
    upload = ProcessedImageField(
        upload_to=_upload_path_for_image,
        height_field=None,
        width_field=None,
        max_length=255,
        processors=[ResizeToFit(width=settings.MAX_IMAGE_SIZE, height=settings.MAX_IMAGE_SIZE)]
    )

    # Whether this image is private (only shows for the uploader) or public.
    private = models.BooleanField(default=False)

    # Whether this image is considered explicit or NSFW, and can thus only be viewed
    # by paying subscribers and moderators/admins.
    nsfw = models.BooleanField(default=False)

    # A status field for the image, to track disputes and moderation reports.
    # By default, an image starts in the moderation queue, but the service layer will
    # auto-accept images from certain sources under certain conditions.
    status = models.CharField(max_length=3, choices=ImageStatus.choices, default=ImageStatus.MODERATED)

    # The user who uploaded the image.
    uploader = models.ForeignKey(User, on_delete=models.CASCADE, related_name="uploaded_images")

    # Unstructured metadata about the image.
    image_meta = models.JSONField(encoder=DjangoJSONEncoder, default=dict, null=True, blank=True)

    # The lexemes associated with this image.
    # Images can be associated with multiple lexemes, but should only have one per language.
    # lexeme = models.ForeignKey("languages.Lexeme", on_delete=models.CASCADE, related_name="images")
    lexemes = models.ManyToManyField("languages.Lexeme", related_name="images", blank=True)
    
    def __str__(self):
        return f"{self.upload.url}:{self.status}"

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("images:image_page", kwargs={"hash_id": self.hash_id})

    @property
    def hash_id(self) -> str:
        return _image_hashid_generator.encode(self.pk)

    @classmethod
    def unhash_id(self, hash_id: str) -> int:
        unhashed = _image_hashid_generator.decode(hash_id)
        if len(unhashed) > 1:
            raise ValueError("Invalid hash ID")
        return unhashed[0]

    def lexemes_of_language(self, language_code: str):
        return self.lexemes.filter(language__language_code=language_code)

    def sentences_of_language(self, language_code: str):
        return self.sentences.filter(language__language_code=language_code)

    @property
    def approved_sentences(self):
        return self.sentences.filter(status=Sentence.SentenceStatus.APPROVED)
