from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from django_extensions.db.models import TimeStampedModel

# Create your models here.

# Django uses a RDBMS, where polymorphic lists are...difficult. Instead,
# we have to make a separate model for each type of list. This does at least
# give us separate namespaces, though.

class ItemList(TimeStampedModel, models.Model):
    """Abstract model for a list of items of a single type."""

    # Name of the list.
    name = models.CharField(max_length=100)

    # The user who created the list.
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="%(class)s_lists")

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class ImageList(ItemList):
    """Model for a list of images."""

    items = models.ManyToManyField("images.Image", related_name="lists", blank=True)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("image_list_detail", kwargs={"pk": self.pk})
    

class SentenceList(ItemList):
    """Model for a list of sentences."""

    items = models.ManyToManyField("sentences.Sentence", related_name="lists", blank=True)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("sentence_list_detail", kwargs={"pk": self.pk})


class LexemeList(ItemList):
    """Model for a list of lexemes."""

    items = models.ManyToManyField("languages.Lexeme", related_name="lists", blank=True)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse("lexeme_list_detail", kwargs={"pk": self.pk})
