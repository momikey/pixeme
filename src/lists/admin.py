from django.contrib import admin

from .models import ImageList, LexemeList, SentenceList

# Register your models here.
admin.site.register([ImageList, LexemeList, SentenceList])
